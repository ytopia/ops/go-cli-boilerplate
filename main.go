package main

import (
	"gitlab.com/youtopia.earth/ops/snip/app"
)

func main() {
	app.New()
}
